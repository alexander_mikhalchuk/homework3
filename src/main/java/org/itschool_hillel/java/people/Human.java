package org.itschool_hillel.java.people;

public class Human {
    protected String name;
    protected Sex sex;

    public Human(String name, Sex sex) {
        this.name = name;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void eat(String food) {
    }

    @Deprecated
    public void smoke() {
    }
}
