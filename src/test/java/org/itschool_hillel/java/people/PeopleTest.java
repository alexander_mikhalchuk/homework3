package org.itschool_hillel.java.people;

import org.junit.Assert;
import org.junit.Test;

public class PeopleTest {

    @Test
    public void testHumanConstructor() {
        Woman eva = new Woman("Eva");
        Man adam = new Man("Adam");

        Assert.assertEquals(Sex.MAN, adam.sex);
        Assert.assertEquals(Sex.WOMAN, eva.sex);

        adam.eat("una manzana");
        eva.eat("an apple");

        adam.smoke();

        Human baby = eva.born(adam);

        if (baby instanceof Man) {
            Assert.assertEquals(eva.name + " " + adam.name, baby.name);
        }

        if (baby instanceof Woman) {
            Assert.assertEquals(adam.name + " " + eva.name, baby.name);
        }
    }
}
