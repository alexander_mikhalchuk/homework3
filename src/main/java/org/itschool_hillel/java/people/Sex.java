package org.itschool_hillel.java.people;

public enum Sex {
    MAN, WOMAN
}
