package org.itschool_hillel.java.people;

public class Man extends Human {
    public Man(String name) {
        super(name, Sex.MAN);
    }
}
